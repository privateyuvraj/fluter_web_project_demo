import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

part 'system_state.dart';

class SystemCubit extends Cubit<SystemState> {
  SystemCubit()
      : super(const SystemState(
          themeMode: ThemeMode.system,
          showTopSignUp: false,
        ));

  void changeThemeMode(ThemeMode themeMode) {
    emit(state.copyWith(themeMode: themeMode));
  }

  void changeShowTopSignUp(bool showTopSignUp) {
    emit(state.copyWith(showTopSignUp: showTopSignUp));
  }
}
