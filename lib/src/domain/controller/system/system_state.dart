part of 'system_cubit.dart';

class SystemState extends Equatable {
  const SystemState({
    this.themeMode = ThemeMode.system,
    required this.showTopSignUp,
  });
  final ThemeMode themeMode;
  final bool showTopSignUp;

  @override
  List<Object?> get props => [themeMode, showTopSignUp];

  SystemState copyWith({
    ThemeMode? themeMode,
    bool? showTopSignUp,
  }) =>
      SystemState(
        themeMode: themeMode ?? this.themeMode,
        showTopSignUp: showTopSignUp ?? this.showTopSignUp,
      );
}
