import 'package:flutter/material.dart';
import '../../../../../../src/core/extension/extension.dart';

import 'package:flutter_svg/flutter_svg.dart';

import '../../../core/constants/ui/ui.dart';
import '../buttons/btn_widget.dart';

class HeroSection extends StatelessWidget {
  const HeroSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: context.mq.size.width,
      height: context.isMobile
          ? context.mq.size.height
          : context.mq.size.height * .75,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xFFE6FFFA),
            Color(0xFFEBF4FF),
          ],
        ),
      ),
      child: Stack(
        fit: StackFit.expand,
        children: [
          const Positioned.fill(
            child: WaveShapeBackgroundWidget(),
          ),
          Positioned(
            top: context.isMobile ? kToolbarHeight * 1.5 : kToolbarHeight * 2,
            left: 0,
            right: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  crossAxisAlignment: context.isMobile
                      ? CrossAxisAlignment.center
                      : CrossAxisAlignment.center,
                  children: [
                    const Text(
                      'Deine Job \nwebsite',
                      style:
                          TextStyle(fontSize: 48, fontWeight: FontWeight.bold),
                    ),
                    context.isMobile
                        ? const SizedBox(height: 20)
                        : const SizedBox(),
                    if (!context.isMobile)
                      BtnWidget(
                        labelText: 'Kostenlos Registrieren',
                        margin: EdgeInsets.only(
                          top: context.isMobile ? 130 : 20,
                          left: 20,
                        ),
                        width: context.isMobile
                            ? context.mq.size.width * .9
                            : context.mq.size.width * 0.2,
                        gradient: LinearGradient(
                          colors: [
                            $color.color1,
                            $color.color2,
                          ],
                        ),
                        textStyle: context.theme.textTheme.labelLarge?.copyWith(
                          color: Colors.white,
                        ),
                        onPressed: () {},
                      ),
                  ],
                ),
                const SizedBox(width: 20),
                AnimatedSwitcher(
                  duration: const Duration(milliseconds: 300),
                  child: context.isDesktop || context.isTablet
                      ? ClipOval(
                          child: Container(
                            width: 400,
                            height: 400,
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle,
                            ),
                            child: SvgPicture.asset(
                              'assets/svg/0.svg',
                              fit: BoxFit.cover,
                              clipBehavior: Clip.antiAlias,
                            ),
                          ),
                        )
                      : const SizedBox(),
                ),
              ],
            ),
          ),
          Positioned.fill(
            top: kToolbarHeight * 3.7,
            left: -30,
            right: -30,
            child: context.isMobile
                ? SvgPicture.asset(
                    'assets/svg/0.svg',
                    width: context.mq.size.width * .9,
                    fit: BoxFit.cover,
                    clipBehavior: Clip.antiAlias,
                  )
                : const SizedBox(),
          ),
        ],
      ),
    );
  }
}

class WaveShapeBackgroundWidget extends StatelessWidget {
  const WaveShapeBackgroundWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: _WaveShapeClipper(),
      child: Container(
        color: Colors.white,
      ),
    );
  }
}

class _WaveShapeClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path()
      ..moveTo(0, size.height * 0.75)
      ..lineTo(size.width * 0.2, size.height * 0.75)
      ..quadraticBezierTo(
          size.width * 0.7, size.height * 0.7, size.width, size.height * 0.7)
      ..lineTo(size.width, size.height)
      ..lineTo(0, size.height)
      ..close();
    return path;
  }

  @override
  bool shouldReclip(_WaveShapeClipper oldClipper) => false;
}
