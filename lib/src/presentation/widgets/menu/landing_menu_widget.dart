import 'package:flutter/material.dart';
import '../../../../../../src/core/extension/extension.dart';
import '../../../core/constants/ui/ui.dart';

class LandingMenuWidget extends StatelessWidget {
  const LandingMenuWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: context.mq.size.width,
          height: 5,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                $color.color2,
                $color.color1,
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all($style.kPadding),
          width: context.mq.size.width,
          height: kToolbarHeight,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular($style.kBorderRadiusL),
              bottomRight: Radius.circular($style.kBorderRadiusL),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 10,
                offset: Offset(0, 5),
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(),
              Row(
                children: [
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      'Login',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: $color.color1,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
