import 'package:flutter/material.dart';
import '../../../../../../src/core/extension/extension.dart';
import '../../../core/constants/ui/ui.dart';

class BtnWidget extends StatelessWidget {
  const BtnWidget({
    super.key,
    this.padding,
    this.margin,
    this.gradient,
    this.labelText,
    this.backgroundColor,
    this.textStyle,
    this.width,
    this.height,
    required this.onPressed,
  });
  final EdgeInsetsGeometry? padding, margin;
  final String? labelText;
  final Color? backgroundColor;
  final VoidCallback onPressed;
  final Gradient? gradient;
  final TextStyle? textStyle;
  final double? width, height;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        margin: margin ?? EdgeInsets.zero,
        padding: padding ?? EdgeInsets.all($style.kPadding),
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: backgroundColor,
          gradient: gradient ??
              LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  backgroundColor ?? Colors.white,
                  backgroundColor ?? Colors.white,
                ],
              ),
          borderRadius: BorderRadius.circular($style.kBorderRadius),
        ),
        child: Text(
          labelText ?? 'Button',
          textAlign: TextAlign.center,
          style: textStyle ?? context.theme.textTheme.labelLarge?.copyWith(),
        ),
      ),
    );
  }
}
