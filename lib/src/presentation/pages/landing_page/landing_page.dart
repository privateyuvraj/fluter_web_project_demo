import 'package:flutter/material.dart';
import '../../../../../../src/core/extension/extension.dart';

import '../../../core/constants/ui/ui.dart';
import '../../../core/di/app_init.dart';
import '../../../domain/controller/controller.dart';
import '../../widgets/buttons/btn_widget.dart';
import '../../widgets/widgets.dart';
import 'components/components.dart';

import 'package:flutter_animate/flutter_animate.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({super.key});
  static const routeName = '/';

  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    _scrollController.addListener(() {
      if (_scrollController.offset > 300 && !context.isMobile) {
        $getIt<SystemCubit>().changeShowTopSignUp(false);
      }
      if (_scrollController.offset < 300 && !context.isMobile) {
        $getIt<SystemCubit>().changeShowTopSignUp(true);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      bottomNavigationBar: context.isMobile
          ? Container(
              padding: const EdgeInsets.all(20),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 10,
                  ),
                ],
              ),
              child: BtnWidget(
                labelText: 'Kostenlos Registrieren',
                margin: const EdgeInsets.only(),
                width: context.isMobile
                    ? context.mq.size.width * .9
                    : context.mq.size.width * 0.2,
                gradient: LinearGradient(
                  colors: [
                    $color.color1,
                    $color.color2,
                  ],
                ),
                textStyle: context.theme.textTheme.labelLarge?.copyWith(
                  color: Colors.white,
                ),
                onPressed: () {},
              ),
            )
          : null,
      body: Stack(
        children: [
          SingleChildScrollView(
            controller: _scrollController,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const LandingHeaderSection(),
                SizedBox(height: $style.kPaddingXXL),
                RoundedTabBar(
                  tabs: const [
                    Tab(text: 'Arbeitnehmer'),
                    Tab(text: 'Arbeitgeber'),
                    Tab(text: 'Temporärbüro'),
                  ],
                  controller: _tabController,
                  onChanged: (value) {
                    setState(() {
                      _tabController.index = value;
                    });
                  },
                ),
                SizedBox(height: $style.kPaddingXXL),
                AnimatedSwitcher(
                  duration: const Duration(milliseconds: 300),
                  child: _tabController.index == 0
                      ? const WorkerSection()
                      : _tabController.index == 1
                          ? const EmployerSection()
                          : const OfficeSection(),
                ),
              ],
            ),
          ),
          Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: const LandingMenuWidget().animate().fade().slide()),
        ],
      ),
    );
  }
}

class RoundedTabBar extends StatefulWidget {
  const RoundedTabBar({
    super.key,
    required this.tabs,
    required this.controller,
    required this.onChanged,
  });

  final List<Tab> tabs;
  final TabController controller;
  final ValueChanged<int> onChanged;

  @override
  State<RoundedTabBar> createState() => _RoundedTabBarState();
}

class _RoundedTabBarState extends State<RoundedTabBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal:
              context.isMobile ? $style.kMargin : context.mq.size.width * 0.2),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular($style.kBorderRadius),
      ),
      child: TabBar(
        controller: widget.controller,
        tabs: widget.tabs,
        labelColor: Colors.white,
        unselectedLabelColor: $color.color1,
        indicator: BoxDecoration(
          borderRadius: (widget.controller.index == 0)
              ? BorderRadius.only(
                  topLeft: Radius.circular($style.kBorderRadius),
                  bottomLeft: Radius.circular($style.kBorderRadius),
                )
              : (widget.controller.index == widget.tabs.length - 1)
                  ? BorderRadius.only(
                      topRight: Radius.circular($style.kBorderRadius),
                      bottomRight: Radius.circular($style.kBorderRadius),
                    )
                  : BorderRadius.zero,
          color: $color.color1,
        ),
        onTap: (value) {
          widget.onChanged(value);
        },
      ),
    );
  }
}
