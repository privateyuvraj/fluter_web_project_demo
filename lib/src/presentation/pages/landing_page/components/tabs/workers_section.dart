import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../../../../src/core/extension/extension.dart';

import '../../../../../core/constants/ui/ui.dart';

class WorkerSection extends StatefulWidget {
  const WorkerSection({super.key});

  @override
  State<WorkerSection> createState() => _WorkerSectionState();
}

class _WorkerSectionState extends State<WorkerSection> {
  final GlobalKey _key1 = GlobalKey();
  final GlobalKey _key2 = GlobalKey();
  final GlobalKey _key3 = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(
        children: [
          Column(
            children: [
              Text(
                'Drei einfache Schritte \nzur Vermittlung neuer Mitarbeiter',
                textAlign: TextAlign.center,
                style: context.theme.textTheme.displayMedium?.copyWith(
                  fontWeight: FontWeight.bold,
                  fontSize: 40,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 100,
                  horizontal: context.isMobile ? 20 : 0,
                ),
                child: Wrap(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  alignment: WrapAlignment.end,
                  children: [
                    if (context.isMobile)
                      SvgPicture.asset('assets/svg/1.svg', height: 180),
                    RichText(
                      key: _key1,
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: '1.',
                            style:
                                context.theme.textTheme.displayLarge?.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text: 'Erstellen dein Lebenslauf',
                            style: context.theme.textTheme.titleLarge?.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 20),
                    if (!context.isMobile) SvgPicture.asset('assets/svg/1.svg'),
                  ],
                ),
              ),
              Stack(
                children: [
                  SizedBox(
                    child: Positioned(
                      left: 0,
                      right: 0,
                      child: SizedBox(
                        child: ShaderMask(
                          shaderCallback: (bounds) => LinearGradient(
                            colors: $color.bgGradientColor,
                          ).createShader(bounds),
                          child: context.isMobile
                              ? SvgPicture.asset(
                                  'assets/svg/bg.svg',
                                  width: context.mq.size.width,
                                  height: 400,
                                  fit: BoxFit.cover,
                                  colorFilter: const ColorFilter.mode(
                                    Colors.white,
                                    BlendMode.srcIn,
                                  ),
                                )
                              : SvgPicture.asset(
                                  'assets/svg/bg.svg',
                                  width: context.mq.size.width,
                                  fit: BoxFit.cover,
                                  colorFilter: const ColorFilter.mode(
                                    Colors.white,
                                    BlendMode.srcIn,
                                  ),
                                ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 150,
                      right: context.isMobile ? 20 : 0,
                      left: context.isMobile ? 20 : 0,
                    ),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            if (!context.isMobile)
                              SvgPicture.asset('assets/svg/2.svg'),
                            const SizedBox(width: 20),
                            RichText(
                              key: _key2,
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: '2.',
                                    style: context.theme.textTheme.displayLarge
                                        ?.copyWith(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  TextSpan(
                                    text: 'Erstellen dein Lebenslauf',
                                    style: context.theme.textTheme.titleLarge
                                        ?.copyWith(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        if (context.isMobile)
                          SvgPicture.asset('assets/svg/2.svg'),
                      ],
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 150,
                  horizontal: context.isMobile ? 20 : 0,
                ),
                child: Wrap(
                  children: [
                    Stack(
                      children: [
                        const CircleAvatar(
                          radius: 70,
                          backgroundColor: Colors.black12,
                        ),
                        RichText(
                          key: _key3,
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: '3.',
                                style: context.theme.textTheme.displayLarge
                                    ?.copyWith(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(
                                text: 'Mit nur einem Klick bewerben',
                                style: context.theme.textTheme.titleLarge
                                    ?.copyWith(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(width: 20),
                    SvgPicture.asset(
                      'assets/svg/3.svg',
                      fit: BoxFit.cover,
                      width: 400,
                    ),
                  ],
                ),
              ),
            ],
          ),
          if (!context.isMobile)
            Center(
              child: Container(
                margin: const EdgeInsets.only(top: 450, left: 0),
                child: Transform.flip(
                  flipX: true,
                  child: SvgPicture.asset(
                    'assets/svg/arrow.svg',
                  ),
                ),
              ),
            ),
          if (!context.isMobile)
            Center(
              child: Container(
                margin: const EdgeInsets.only(top: (260 + 200) * 2, left: 0),
                child: SvgPicture.asset(
                  'assets/svg/arrow.svg',
                ),
              ),
            ),
        ],
      ),
    );
  }
}
