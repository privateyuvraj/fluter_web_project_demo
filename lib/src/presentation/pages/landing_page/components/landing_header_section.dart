import 'package:flutter/material.dart';

import '../../../widgets/widgets.dart';

class LandingHeaderSection extends StatefulWidget {
  const LandingHeaderSection({super.key});

  @override
  State<LandingHeaderSection> createState() => _LandingHeaderSectionState();
}

class _LandingHeaderSectionState extends State<LandingHeaderSection> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const HeroSection(),
      ],
    );
  }
}
