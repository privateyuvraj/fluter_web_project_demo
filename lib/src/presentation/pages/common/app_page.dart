import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../core/constants/ui/ui.dart';
import '../../../core/di/app_init.dart';
import '../../../core/routes/routes.dart';
import '../../../domain/controller/controller.dart';

class AppPage extends StatefulWidget {
  const AppPage({super.key});

  @override
  State<AppPage> createState() => _AppPageState();
}

class _AppPageState extends State<AppPage> {
  final cubit = $getIt<SystemCubit>();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SystemCubit, SystemState>(
      bloc: cubit,
      listener: (context, state) {},
      builder: (context, state) {
        return MaterialApp.router(
          title: 'Flutter Demo',
          debugShowCheckedModeBanner: false,
          theme: $style.lightTheme,
          darkTheme: $style.darkTheme,
          themeMode: state.themeMode,
          routerConfig: $routes,
        );
      },
    );
  }
}
