import 'package:get_it/get_it.dart';

import '../../domain/controller/system/system_cubit.dart';

final $getIt = AppInit.getIt;

class AppInit {
  AppInit._();
  static late GetIt getIt;

  void reset() async {
    await getIt.unregister();
  }

  static void close() {
    getIt.reset();
  }

  static dynamic init() {
    getIt = GetIt.I;
    create();
  }

  static void create() async {
    _initCubit();
  }

  static void _initCubit() {
    getIt.registerLazySingleton(SystemCubit.new);
  }
}
