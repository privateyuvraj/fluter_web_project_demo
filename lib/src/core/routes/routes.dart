import 'package:go_router/go_router.dart';

import '../../presentation/pages/pages.dart';
import '../observers/navigation_observer.dart';

final $routes = GoRouter(
  initialLocation: LandingPage.routeName,
  observers: [
    NavigationObserver(),
  ],
  routes: [
    GoRoute(
      path: LandingPage.routeName,
      builder: (context, state) => const LandingPage(),
    ),
  ],
);
