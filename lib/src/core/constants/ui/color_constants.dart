import 'package:flutter/material.dart';

final $color = AppColors();

class AppColors {
  AppColors();
  final color1 = const Color(0xFF319795);
  final color2 = const Color(0xFF3182CE);
  final color3 = const Color(0xFF81E6D9);
  final color4 = const Color(0xFFF7FAFC);
  final color5 = const Color(0xFF000029);

  final bgGradientColor = [const Color(0xFFE6FFFA), const Color(0xFFEBF4FF)];
}
