import 'package:flutter/material.dart';

final $style = AppStyle();

class AppStyle {
  AppStyle();

  /// [Padding] Section
  final kPadding = 16.0;
  final kPaddingXs = 4.0;
  final kPaddingS = 8.0;
  final kPaddingM = 16.0;
  final kPaddingL = 24.0;
  final kPaddingXL = 32.0;
  final kPaddingXXL = 48.0;

  /// [Margin] Section
  final kMargin = 16.0;
  final kMarginXs = 4.0;
  final kMarginS = 8.0;
  final kMarginM = 16.0;
  final kMarginL = 24.0;
  final kMarginXL = 32.0;
  final kMarginXXL = 48.0;

  /// [Border Radius] Section
  final kBorderRadius = 8.0;
  final kBorderRadiusXs = 4.0;
  final kBorderRadiusS = 8.0;
  final kBorderRadiusM = 16.0;
  final kBorderRadiusL = 24.0;
  final kBorderRadiusXL = 32.0;
  final kBorderRadiusXXL = 48.0;

  /// [lightTheme] Section
  final lightTheme = ThemeData(
    brightness: Brightness.light,
    fontFamily: 'MyFontFamily',
  );

  /// [darkTheme] Section
  final darkTheme = ThemeData(
    brightness: Brightness.dark,
    fontFamily: 'MyFontFamily',
  );
}
