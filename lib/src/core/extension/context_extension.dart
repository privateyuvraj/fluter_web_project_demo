import 'package:flutter/material.dart';

extension ContextHelper on BuildContext {
  /// Return MediaQuery.of(context) instance
  MediaQueryData get mq => MediaQuery.of(this);

  /// width below [650] can be considered as mobile platform
  bool get isMobile => mq.size.width < 650;

  /// width in between [650] and [1100] can be considered as tablet platform
  bool get isTablet => mq.size.width < 1100 && mq.size.width >= 650;

  /// width above [1100] can be considered as desktop platform
  bool get isDesktop => mq.size.width >= 1100;

  /// Return Theme.of(context) instance
  ThemeData get theme => Theme.of(this);
}
