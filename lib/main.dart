import 'package:flutter/material.dart';

import 'src/core/configs/config_web.dart';
import 'src/core/di/app_init.dart';
import 'src/presentation/pages/pages.dart';

void main() async {
  /// This is required to ensure that the initialization of the
  WidgetsFlutterBinding.ensureInitialized();

  /// configure the app to use the path strategy
  configureApp();

  /// initialize the app
  await AppInit.init();

  /// run the app
  runApp(const AppPage());
}
